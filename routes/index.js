var ctrl = require('../controllers');

exports = module.exports = function(app){
    app.get('/', ctrl.userCtrl.welcome);
    app.post('/user/login', ctrl.userCtrl.login);
    app.post('/user/logout', ctrl.userCtrl.logout);
    app.post('/user/signup', ctrl.userCtrl.register);
    app.post('/user/auth', ctrl.userCtrl.auth);
    app.get('/user/font/all', ctrl.userCtrl.getUserFontList);
    app.get('/user/config', ctrl.userCtrl.getUserDefaultConfig);
    app.get('/font/all', ctrl.fontCtrl.getFontList);
    app.post('/font/generateJSCode', ctrl.fontCtrl.generateJSCode);
    app.post('/font/generateFont', ctrl.fontCtrl.generateFont);
    app.post('/pay', ctrl.payCtrl.pay);

    // pre auth
    function authUser(req, res, next){
        req.session.userinfo = req.session.userinfo || {isLogin: false};
        if(req.session.userinfo.isLogin){
            next();
        }else{
            var data = {
                feedback: {
                    type: 'authFailure',
                    message: '请先登录'
                }
            };
            res.send(data);
        }
    }
};
