var config = require('../config');
var Mysql = config.db.connect();
var FontUserRelation = require('./fontUserRelationModel');
var Font = require('./fontModel');

exports = module.exports = Mysql.Model.extend({
    tableName: 'User',

    fontUserRelation: function(){
        return this.hasMany(FontUserRelation);
    },

    font: function(){
        return this.belongsToMany(Font).through(FontUserRelation);
    }
});
