var config = require('../config');
var Mysql = config.db.connect();
var FontUserRelation = require('./fontUserRelationModel');
var User = require('./userModel');

exports = module.exports = Mysql.Model.extend({
    tableName: 'Font',

    user: function(){
        return this.belongsToMany(User).through(FontUserRelation);
    }
});
