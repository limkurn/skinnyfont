var fs = require('fs');
var _ = require('underscore');
var Q = require('Q');
var path = require('path');
var md5 = require('MD5');
var exec = require('child_process').exec;
var uuid = require('node-uuid');
var config = require('../config');
var Mysql = config.db.connect();
var settings = require('../config').settings;
var logger = require('../config').logger;
var Font = require('../models').font;
var FontUserConfig = require('../models').fontUserConfig;
var fs = require('fs');

var feedback = {
    type: '',
    message: ''
};

exports = module.exports = {
    getFontList: function(req, res){
        logger.info('Start to getting font list');
        var data = {};

        var dataHandler = function(){
            var defferred = Q.defer();
            Font.collection().fetch().then(function(collection) {
                if(_.isUndefined(collection)){
                    feedback.message = '字体不存在';
                    logger.error('font search faillingly, the font is not exist');
                    defferred.reject(new Error("字体不存在"));
                }
                feedback.fontList = collection;
                defferred.resolve(collection);
            });
            return defferred.promise;
        }

        dataHandler().then(function(collection){
            feedback.type = 'success';
            feedback.message = '查询字体列表成功';
        }).fail(function(err){
            feedback.type = 'failure';
            feedback.message = err;
        }).done(function(){
            data.feedback = feedback;
            res.send(data);
        })
    },

    generateJSCode: function(req, res){
        var host = req.body.host || '';
        var fontList = req.body.fontList || [];
        var user = req.body.user || {};
        var cssClassList = _.pluck(fontList, 'class');
        var generateFontConfig = {
            timeout: 4000,
            method: 'POST'
        };
        var data = {};

        var FontUserConfigs = Mysql.Collection.extend({
            model: FontUserConfig
        });

        //TODO Database Transaction
        // delete origin userFontConfig
        FontUserConfig.collection().query('where', 'User_id', '=', user.id).fetch().then(function(deleteUserConfigList){
            deleteUserConfigList.invoke('destroy');
        });

        // save userFontConfig
        var fontUserConfigList = _.map(fontList, function(font, index){
            return {
                User_id: user.id,
                Font_id: font.id,
                class: font.class,
                host: host
            }
        });
        var fontUserConfigs = FontUserConfigs.forge(fontUserConfigList);
        fontUserConfigs.invoke('save');

        var buildCssParameter = function(cssList){
            return _.map(cssList, function(css){
                return '"' + css + '"';
            });
        }

        // format the generate code template
        var jsCodeTemplate = fs.readFileSync(path.join(__dirname, '../config/generate-code.html'), {'encoding':'utf8'});
        var jsCodeTemplate = _.template(jsCodeTemplate);
        var generatedCode = jsCodeTemplate({
            timeout: generateFontConfig.timeout,
            method: generateFontConfig.method,
            url: settings.fontGenerateServer.url,
            cssParameter: buildCssParameter(cssClassList)
        });


        feedback.type = 'success';
        feedback.message = '代码生成成功';
        data.feedback = feedback;
        data.code = generatedCode;
        res.send(data);
    },

    generateFont: function(req, res){
        var responseFontList = [];
        var classObj = req.body.classObj;
        var host = req.host;
        if(_.isUndefined(classObj) || _.keys(classObj).length < 1){
            feedback.type = 'failure';
            feedback.message = 'can not resolve CSS class objects';
            feedback.fontList = [];
            res.send(feedback);
            return;
        }

        // find the user font config
        var findUserFontCollection = function(){
            var defferred = Q.defer();
            FontUserConfig.collection().query('where', 'host', '=', host).fetch({withRelated: ['font']}).then(function(collection) {
                if(_.isUndefined(collection) || collection.length < 1){
                    defferred.reject(new Error('the host has no authority to accesss skinnyfont serivce'));
                }else{
                    defferred.resolve(collection);
                }
            })
            return defferred.promise;
        }

        // exec a command to convert fonts
        var execToGenerateFont = function(collection){
            var defferred = Q.defer();
            var sfnttoolUrl = __dirname + '/../sfnttool.jar';
            var cmd = '';
            var classCount = 0;
            var promiseClassCount = 0;

            // format the command to exec
            _.each(collection.models, function(fontUserConfigModel){
                var curClass = fontUserConfigModel.get('class');
                var font = fontUserConfigModel.relations.font;
                if(_.isUndefined(classObj[curClass]) || classObj[curClass] === ''){
                    return;
                }
                var fontContent = classObj[curClass];
                var originFontUrl = __dirname + '/../origin_font/ttf/' + font.get('font_file_name') + '.' + font.get('font_type');
                var convertedFontName = font.get('font_file_name') + uuid.v1().substr(0, 10) + '.' + font.get('font_type');
                var convertedFontUrl = __dirname + '/../app/convert_font/' + convertedFontName;
                responseFontList.push({
                    font_url: settings.fontGenerateServer.convertedFontUrl + '/' + convertedFontName,
                    font_name: font.get('font_en_name'),
                    font_class: curClass
                });

                cmd = 'java -jar ' + sfnttoolUrl + ' -w -s "' + fontContent +
                    '" ' + originFontUrl + ' ' + convertedFontUrl;

                // exec the convert command
                classCount++;
                exec(cmd, function(){
                    promiseClassCount++;
                    console.log('finished to covert font');
                    if(promiseClassCount === classCount){
                        defferred.resolve(responseFontList);
                    }
                });
            });
            return defferred.promise;
        }

        // promise to exec the generate font function
        var promise1 = findUserFontCollection();
        var promise2 = promise1.then(function(collection){
            return execToGenerateFont(collection);
        })
        promise2.then(function(responseFontList){
            feedback.type = 'success';
            feedback.message = 'convert font successfully';
            feedback.fontList = responseFontList;
            res.send(feedback);
        }).fail(function(err){
            feedback.type = 'failure';
            feedback.message = err;
            feedback.fontList = [];
            res.send(feedback);
        })
    }
}
