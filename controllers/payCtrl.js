var fs = require('fs');
var _ = require('underscore');
var Q = require('Q');
var config = require('../config');
var Mysql = config.db.connect();
var settings = require('../config').settings;
var logger = require('../config').logger;
var User = require('../models').user;
var FontUserRelation = require('../models').fontUserRelation;

var feedback = {
    type: '',
    message: ''
};

exports = module.exports = {
    pay: function(req, res){
        logger.info('Start to pay');
        var user = req.body.user;
        var fontList = req.body.fontList;
        var ip = req.ip;
        var data = {};

        var fontUserRelationList = _.map(fontList, function(font){
            return {
                Font_id: font,
                User_id: user.id
            }
        })
        var FontUserRelations = Mysql.Collection.extend({
            model: FontUserRelation
        });
        var fontUserRelations = FontUserRelations.forge(fontUserRelationList);

        Q.all(fontUserRelations.invoke('save')).then(function(){
            logger.info('pay successfully');
            feedback.type = 'success';
            feedback.message = '付款成功';
        }).fail(function(err){
            feedback.type = 'failure';
            feedback.message = '付款失败';
        }).done(function(){
            data.feedback = feedback;
            res.send(data);
        })
    }
}
