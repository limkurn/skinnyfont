var fs = require('fs');
var _ = require('underscore');
var Q = require('Q');
var md5 = require('MD5');
var settings = require('../config').settings;
var logger = require('../config').logger;
var User = require('../models').user;
var config = require('../config');
var FontUserConfig = require('../models').fontUserConfig;
var Mysql = config.db.connect();

var feedback = {
    type: '',
    message: ''
};

exports = module.exports = {
    welcome: function(req, res){

    },

    auth: function(req, res){
        req.session.userinfo = req.session.userinfo || {isLogin: false};
        if(req.session['userinfo'].isLogin){
            feedback.type = 'success';
            feedback.message = '自动登录成功';
            feedback.user = {
                mail: req.session.userinfo.mail || '',
                id: req.session.userinfo.id || 0
            }
        }else{
            feedback.type = 'failure';
            feedback.message = '自动登录失败';
        }
        var data = {};
        data.feedback = feedback;
        res.send(data);
    },

    getUserFontList: function(req, res){
        logger.info('Start to getting user font list');
        var currentUserId = req.session.userinfo.id;
        var data = {};

        var dataHandler = function(){
            var defferred = Q.defer();
            new User({id: currentUserId}).related('font').fetch().then(function(collection) {
                if(_.isUndefined(collection)){
                    feedback.message = '字体不存在';
                    logger.error('font search faillingly, the font is not exist');
                    defferred.reject(new Error("字体不存在"));
                }
                feedback.userFontList = collection;
                defferred.resolve(collection);
            });
            return defferred.promise;
        }

        dataHandler().then(function(collection){
            feedback.type = 'success';
            feedback.message = '查询字体列表成功';
        }).fail(function(err){
            feedback.type = 'failure';
            feedback.message = err;
        }).done(function(){
            data.feedback = feedback;
            res.send(data);
        })
    },

    getUserDefaultConfig: function(req, res){
        logger.info('Start to get user default config');

        var currentUserId = req.session.userinfo.id;
        var data = {};

        var dataHandler = function(){
            var defferred = Q.defer();

            FontUserConfig.collection().query('where', 'User_id', '=', currentUserId).fetch().then(function(defaultUserConfigs){
                if(_.isUndefined(defaultUserConfigs)){
                    feedback.message = '用户配置读取失败';
                    logger.error('user font default config search faillingly, there is no data');
                    defferred.reject(new Error("用户配置读取失败"));
                }
                feedback.userDefaultConfigList = defaultUserConfigs;
                defferred.resolve(defaultUserConfigs);
            });
            return defferred.promise;
        }

        dataHandler().then(function(defaultUserConfigs){
            feedback.type = 'success';
            feedback.message = '查询用户配置成功';
        }).fail(function(err){
            feedback.type = 'failure';
            feedback.message = err;
        }).done(function(){
            data.feedback = feedback;
            res.send(data);
        })
    },


    login: function(req, res){
        logger.info('Start to login');
        var mail = req.body.username;
        var password = req.body.password;
        var storedSession = true; //req.body.storedSession;
        var ip = req.ip;
        var data = {};

        var dataHandler = function(){
            var defferred = Q.defer();
            new User({
                'mail': mail
            }).fetch().then(function(model, err) {
                if(_.isUndefined(model)){
                    feedback.message = '此用户不存在';
                    logger.error('Login faillingly, the account is not exist, and the user ip is: ' + ip);
                    defferred.reject(new Error("此用户不存在"));
                }else if(model.get('password') !== password){
                    feedback.message = '密码错误';
                    logger.error('Login faillingly, the password is wrong, and the user ip is: ' + ip);
                    defferred.reject(new Error('用户密码错误'));
                }else if(storedSession){
                    req.session.userinfo = {
                        mail: mail,
                        id: model.get('id'),
                        isLogin: true
                    }
                }else{
                    req.session.userinfo = {
                        mail: mail,
                        id: model.get('id'),
                        isLogin: false
                    }
                }
                feedback.user = {
                    mail: model.get('mail'),
                    id: model.get('id')
                }
                defferred.resolve(model);
            });
            return defferred.promise;
        }

        if(_.isString(mail) && _.isString(password)){
            dataHandler().then(function(model){
                feedback.type = 'success';
                feedback.message = '登陆成功';
            }).fail(function(err){
                feedback.type = 'failure';
                feedback.message = err;
            }).done(function(){
                data.feedback = feedback;
                res.send(data);
            })
        }else{
            logger.error('Login faillingly, the account or password is invalid, and the user ip is: ' + ip);
            feedback.type = 'failure';
            feedback.message = '用户名或密码错误';
            res.send(data);
        }
    },

    register: function(req, res){
        logger.info('Start to register');
        var mail = req.body.username;
        var password = req.body.password;
        var ip = req.ip;

        var data = {};

        var dataHandler = function(){
            var defferred = Q.defer();
            User.forge({
                'mail': mail,
                'password': password
            }).save().then(function(model, err) {
                if(_.isUndefined(model)){
                    defferred.reject(new Error("注册失败"))
                }
                defferred.resolve(model);
            });
            return defferred.promise;
        }

        if(_.isString(mail) && _.isString(password)){
            dataHandler().then(function(model){
                feedback.type = 'success';
                feedback.message = '注册成功';
                console.log(model.get('mail'));
            }).fail(function(err){
                feedback.type = 'failure';
                feedback.message = err;
            }).done(function(){
                data.feedback = feedback;
                res.send(data);
            })
        }else{
            logger.error('Register faillingly, the account or password is invalid, and the user ip is: ' + ip);
            feedback.type = 'failure';
            feedback.message = '用户名或密码不合法';
            data.feedback = feedback;
            res.send(data);
        }
    },

    logout: function(req, res){
        logger.info('Start to logout');
        var data = {};
        req.session.userinfo = {
            isLogin: false
        }
        feedback.type = 'success';
        feedback.message = '登出成功';
        data.feedback = feedback;
        res.send(data);
    }
}
